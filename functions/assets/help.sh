# Help

--help(){
echo "`tput bold`# Docs Shell usage #`tput sgr0`
`tput bold`- Installing:`tput sgr0`
  $ docs access                                               # Grant read/execute permissions to Docs Shell
  $ docs install                                              # Check and install GitLab Docs' dependencies
  $ docs paths | --reset                                      # Set up custom paths or reset to default (cloned) paths
`tput bold`- Test Docs Shell installation:`tput sgr0`
  $ docs hello                                                # Test docs command
  $ docs test files                                           # Test linked files
`tput bold`- Checking dependencies:`tput sgr0`
  $ docs dep                                                  # Check dependencies
  $ docs dep --update                                         # Check and update dependencies
`tput bold`- Compiling:`tput sgr0`
  $ docs compile                                              # Checkout GitLab Docs master, pull, compile docs
`tput bold`- Recompiling:`tput sgr0`
  $ docs recompile                                            # Recompile docs
`tput bold`- Live preview:`tput sgr0`
  $ docs live                                                 # Live-preview docs
`tput bold`- Changing files:`tput sgr0`
  $ docs branch                                               # Creates a new branch in the repo currently open in the terminal
  $ docs branch [repo]                                        # Creates a new branch in the [repo]
                                                                Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell]
  $ docs commit                                               # Add and commit to the repo currently open in the terminal
  $ docs commit -p                                            # Add, commit, and push to the repo currently open in the terminal
  $ docs commit [repo]                                        # Add and commit to the [repo]
  $ docs commit [repo] -p                                     # Add, commit, and push to the [repo]
                                                                Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell]
`tput bold`- Linting:`tput sgr0`
  $ docs lint                                                 # Content (all repos, all linters), and Nanoc (all linters)
  $ docs lint -c | --content                                  # Content only (all repos, all content linters)
  $ docs lint -c [repo]                                       # Content, specific repo, all linters
                                                                Repos: [gitlab], [omnibus], [runner], [charts]
  $ docs lint -c [repo] [content linter]                      # Content, specific repo and specific linter
                                                                Content linters: [markdown], [vale]
  $ docs lint -c gitlab views                                 # Content linter available for GitLab only
  $ docs lint -n | --nanoc                                    # Nanoc only (all linters)
                                                                Nanoc linters: [links], [anchors], [elinks]
  $ docs lint -n [linter]                                     # Nanoc, specific linter
  $ docs lint -c [repo] [content linter] -n [nanoc linter]    # Content (specific repo and specific linter) + Nanoc (all linter or specific linter)

  $ docs lint -d | --dev                                      # Development, all linters
  $ docs lint -d [linter]                                     # Development, specific linter
                                                                Development linters: [jest], [scss], [yaml], [yarn], [rspecs]
  $ docs reset [repo]                                         # Reset repository
                                                                Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell]
"
}
