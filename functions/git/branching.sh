# Script for ZSH

# Test files (docs test files)
test_branching() {
  echo 'functions/git/branching.sh ok'
}

# ---------------------------------------------------------------- #

# Create new branch from updated master

branch() {
  if [[ $1 ]]; then
    go_$1
  fi
  echo `tput setaf 3`"Enter a name for your new branch`tput sgr0` (allowed only: numbers, lowercase letters, dash, underscore):"
  read BRANCH_NAME
  echo `tput bold` "$ git checkout master && git pull origin master" `tput sgr0` ; git checkout master && git pull origin master
  echo `tput bold` "$ git checkout -b $BRANCH_NAME" `tput sgr0` ; git checkout -b $BRANCH_NAME
}

# Commit all and push to current repo and branch
commit() {
  if [[ $1 && $1 != "-p" ]]; then
    go_$1
  fi
  CURRENT_BRANCH=$(git branch --show-current)
  echo `tput setaf 5`"Staging changes to the branch: `tput sgr0 ; tput bold`$CURRENT_BRANCH"`tput sgr0`
  git add .
  echo `tput setaf 3`"Enter a commit message:"`tput sgr0`
  read COMMIT_MSG
  git commit -m "$COMMIT_MSG"
  if [[ $1 == "-p" || $2 == "-p" ]]; then
    git push origin $CURRENT_BRANCH
  fi
}
