# GitLab Docs Shell

A command line interface to work locally on [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs).

## What it does

Introduces an easier and faster way to work on GitLab Documentation through
`docs [options]` commands.

With `docs` commands, you can:

- Clone all repos (GitLab Docs, GitLab, Omnibus, Runner, and Charts) or set up the local paths to your repos (GDK is supported). (`docs install`)
- Check, install, and update GitLab Docs dependencies. (`docs dep`, `docs dep --update`)
- Pull all repos in one command and housekeep to keep them clean. (`docs pull`)
- Compile GitLab Docs and live preview. (`docs compile live`) <!-- To-do: preview on mobile. -->
- Run all linters. (`docs lint`)

See [usage](#usage) for more options.

## Who's this for

- New contributors to GitLab Docs - the easiest way to set it up to work locally.
- Current contributors - the easiest way to update, compile, recompile, and lint GitLab Docs, and to keep the dependencies up-to-date.

## How to use this project

[Install](#installation) Docs Shell to optimize your workflow for updating,
compiling, linting, and previewing GitLab Docs locally.

## Requirements

Docs Shell is optimized for [iTerm2 + ZSH + oh-my-zsh](https://medium.com/ayuth/iterm2-zsh-oh-my-zsh-the-most-power-full-of-terminal-on-macos-bdb2823fb04c) on macOS Catalina. Its behavior on Linux is unknown.

We assume you're familiar with using Git through the command line and GitLab.

### Docs Shell dependency

**ZSH shell is a hard-requirement** for Docs Shell. ZSH comes installed on macOS Catalina.

1. To check which shell you're using, open your terminal and run: `echo $SHELL`.
1. If it returns `/bin/zsh`, proceed to [installation](#installation). If not, check if you have it installed: `zsh --version`.
1. If it returns a version number, such as `zsh 5.7.1`, set ZSH as default shell: `chsh -s /bin/zsh`.

If ZSH is not present in your computer, see [how to install](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH) it according to your OS.

**Note:** if you'd like Docs Shell to install ZSH, iTerm2, and oh-my-zsh, run `docs pre-install` before running `docs install`, so that you can benefit from their features during the installation process. You can also run it at any moment later.

### GitLab Docs dependencies

During the installation process, **all GitLab Docs' dependencies will be installed** (if not present). Mind that:

- NodeJS: Docs Shell will prompt you to install it with Homebrew.
  - Alternative method: [Install Node with a version manager (nvm)](https://github.com/nvm-sh/nvm#installation-and-update).
  - Alternative method: [Install Node directly](https://nodejs.org/en/download/).
- Ruby: Docs Shell will prompt you to install Ruby with [RVM](http://rvm.io/). If you don't use or don't want to use `rvm` to manage Rubies, make sure to install the current [Ruby version](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/master/.ruby-version) of GitLab Docs.

## Installation

To work with Docs Shell, you need to have on your computer all [repos](https://gitlab.com/gitlab-org/gitlab-docs#projects-we-pull-from)
GitLab Docs pull from. During the installation process, Docs Shell will prompt
you to either clone all repos or to set their paths if you already have them
cloned on your computer.

The file tree Docs Shell considers **default** is:

```text
.
├── main-dir
│   ├── docs-shell
│   ├── gitlab-docs
│   ├── gitlab
│   ├── omnibus-gitlab
│   ├── gitlab-runner
│   └── charts
```

`main-dir` can be any directory of your choice.

If you have a different file tree, you only need to clone Docs Shell into a folder of your choice:

```text
.
├── main-dir
│   ├── docs-shell
```

The path to the other repos can be set during the installation process.

**Note:** If you have any problems or questions before and during the
installation process, see [installation details](docs/installation.md).

### Fork and clone Docs Shell

Start by cloning this project onto your computer:

1. From the GitLab UI, [fork](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#fork)
Docs Shell into your namespace.
1. Create a folder in your computer to hold this (or all) repos and [open it in a terminal window](docs/index.md#open-directory-in-a-terminal-window).
1. [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
your fork of the Docs Shell project onto your computer into the `main-dir`.

### Install Docs Shell

1. Grant terminal execution to Docs Shell: [`cd`](docs/index.md#open-directory-in-a-terminal-window) into the `docs-shell` directory and run `. ./docs.sh && access`. Follow the prompt steps.
1. To test it, run: `docs hello`. The expected output is one single line saying hello to you.
1. To install all the dependencies and to set all repos (clone or to set up your existing repos' paths), run: `docs install`.
1. If installed successfully, Docs Shell will open a new browser tab or window previewing GitLab Docs. Give it a few seconds to load. Yay, all done!

See [**installation details**](docs/installation.md) for further info and troubleshooting.

Once installed, check how it works:

1. Open any GitLab document (`gitlab/doc/file.md`) and make a change.
1. Run `docs recompile`. Refresh your browser to see the changes.
1. Open another terminal window (wherever) and run `docs lint -c gitlab` to lint `markdownlint` and `vale` in `gitlab`. Check the output.
1. To stop the live preview process, or to stop the script at any moment,
press <kbd>control</kbd> + <kbd>C</kbd> on your keyboard. You can also run `docs kill` to stop Nanoc live.

## Update Docs Shell

After creating a fork, you need to either update your fork manually or use
[repository mirroring](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository-starter)
to keep your fork up-to-date with the upstream repo.

To update it manually, open a terminal window in the `docs-shell` folder and:

1. Add the path to GitLab Shell as upstream: `git remote add upstream https://gitlab.com/marcia/docs-shell.git`.
1. Update your local repo: `git pull upstream master`.
1. Push to your fork: `git push origin master`.
1. Run `docs dep`.

From Docs Shell 1.0.4 onwards, `docs install` will add the upstream path for
you. To update, run `docs shell --update`, which runs the commands 2, 3, and 4
above.

## Usage

- Check the most common [workflows](docs/index.md#workflows).
- See [all commands available](docs/commands-reference.md).

## Limitations

- Docs Shell is written and tested on masOS Catalina. If you have a different OS, please test it and open an issue or a merge request in this project if you find any problems.
- Previewing docs partially (for example, compile the site with only `gitlab` docs, not including Omnibus, Runner, and Charts) has not been tested, so it can cause unforeseen errors.

## Info

For references, see [info](docs/info.md).

## License

Copyright (c) 2020 [Marcia Ramos](https://gitlab.com/marcia). See [LICENSE](LICENSE) for
further details.

Contributions are very welcome.
