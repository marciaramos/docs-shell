# Script for ZSH

# Test file (docs test files)
test_install_ruby() {
  echo 'install/ruby.sh ok'
}

# ---------------------------------------------------------------- #

# Load current gitlab-docs Ruby (from `master`)

use_ruby(){
  cd $DSHELL
  if [[ $RVMANAGER == 'rvm' ]] ; then
    rvm use $(set_rv) >/dev/null
  elif [[ $RVMANAGER == 'rbenv' ]]; then
    rbenv shell $(set_rv) >/dev/null
  fi
}

# ---------------------------------------------------------------- #

# Check gitlab-docs and system Rubies

# Curl .ruby-version from GitLab Docs master branch
set_rv() {
  curl -s https://gitlab.com/gitlab-org/gitlab-docs/-/raw/master/.ruby-version > .ruby-version
}

# Cat Ruby version
docsrv() {
  set_rv
  cat .ruby-version
}

# Get local Ruby version
localrv() {
  cd $DSHELL
  # get 2nd word | get first 1-5 chars
  ruby --version | cut -d' ' -f2 | cut -c 1-5
}

# ---------------------------------------------------------------- #

# Check RVM

# Install RVM
rvm-install() {
  echo `tput bold` "$ curl -sSL https://get.rvm.io | bash -s stable" `tput sgr0` ; curl -sSL https://get.rvm.io | bash -s stable
}

# Install gitlab-docs Ruby with RVM
rvm-install-ruby() {
  DOCSRUBY=$(docsrv)
  echo `tput bold` "$ rvm install $DOCSRUBY" `tput sgr0` ; rvm install $DOCSRUBY
  echo `tput bold` "$ rvm use $DOCSRUBY" `tput sgr0` ; rvm use $DOCSRUBY
}

# rvm all set (check/install rvm, rvm install Ruby)
rvm-set() {
  if ! depcheck_rvm ; then
    rvm-install
  fi
  if ! depcheck_ruby ; then
    rvm-install-ruby
  fi
  tput setaf 2; printf "Rubies ready! `tput sgr0` (${NOW}) \n"
}

depcheck_rvm() {
  echo `tput setaf 5`"Checking RVM...." `tput sgr0`
  echo `tput bold` '$ rvm --version' `tput sgr0` ; rvm --version
}

check_rvm() {
  if ! depcheck_rvm ; then
    tput setaf 3
    read  "REPLY?Would you like to install RVM (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      rvm-set
    else
      echo "Okay, let's try to proceed without a Ruby Version Manager."
    fi
  else
    # Look for RVMANAGER user variable and replaces with rvm
    if $(grep -q "RVMANAGER" functions/assets/user-variables.sh) ; then
      sed -i "" "s%RVMANAGER='?'%RVMANAGER='rvm'%g" functions/assets/user-variables.sh
    fi
  fi
} 

# ---------------------------------------------------------------- #

# Check rbenv

# Check if rbenv exists and output the current Ruby version
depcheck_rbenv() {
  echo `tput setaf 5`"Checking RBENV..." `tput sgr0`
  echo `tput bold` '$ rbenv version' `tput sgr0` ; rbenv version
}

# If rbenv isn't installed, give option to install RBENV or to use RVM
check_rbenv() {
  if ! depcheck_rbenv ; then
    if ! depcheck_rvm ; then
      tput setaf 3
      read  "REPLY?Recommended: Would you like to install RBENV (y/n)? (Type 'n' to use RVM instead)"
      tput sgr0
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        echo `tput bold` '$ brew install rbenv' `tput sgr0` ; brew install rbenv
        echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
        echo 'eval "$(rbenv init -)"' >> ~/.zshrc
        . ~/.zshrc
        echo "Checking rbenv installation with rbenv-doctor..."
        curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
        ifsuccess
        rbenv_install_ruby
      else
        check_rvm
      fi
    fi
  else
    # Look for RVMANAGER user variable and replaces with rbenv
    if $(grep -q "RVMANAGER" functions/assets/user-variables.sh) ; then
      sed -i "" "s%RVMANAGER='?'%RVMANAGER='rbenv'%g" functions/assets/user-variables.sh
    fi
  fi
}

# Install Docs Ruby version with rbenv, set shell to use that version
rbenv_install_ruby(){
  DOCSRUBY=$(docsrv)
  echo `tput bold` "$ rbenv install $DOCSRUBY" `tput sgr0` ; rbenv install $DOCSRUBY
  rbenv shell $DOCSRUBY >/dev/null
}

# ---------------------------------------------------------------- #

# Define which Ruby version manager

# Add RVMANAGER to user variables
set_rvmanager() {
  if (rbenv version) ; then
    if $(grep -q "RVMANAGER" functions/assets/user-variables.sh) ; then
      sed -i "" "s%RVMANAGER='?'%RVMANAGER='rbenv'%g" functions/assets/user-variables.sh
    else
      echo "RVMANAGER='rbenv'" >> functions/assets/user-variables.sh
    fi
    tput setaf 2; echo "Your Ruby version manager is RBENV." ; tput sgr0
  elif (rvm --version) ; then
    if $(grep -q "RVMANAGER" functions/assets/user-variables.sh) ; then
      sed -i "" "s%RVMANAGER='?'%RVMANAGER='rvm'%g" functions/assets/user-variables.sh
    else
      echo "RVMANAGER='rvm'" >> functions/assets/user-variables.sh
    fi
    tput setaf 2; echo "Your Ruby version manager is RVM." ; tput sgr0
  else
    if $(grep -q "RVMANAGER" functions/assets/user-variables.sh) ; then
      sed -i "" "/RVMANAGER/d" functions/assets/user-variables.sh
    fi
    echo "Consider installing RBENV or RVM as a Ruby version manager."
  fi
}

# ---------------------------------------------------------------- #

# Check Ruby

# Check if Ruby is installed and use gitlab-docs Ruby
depcheck_ruby() {
  DOCSRUBY=$(docsrv)
  echo `tput setaf 5`"Checking Ruby version..."`tput sgr0`
  docsrv
  if [[ $RVMANAGER == 'rvm' ]] ; then
    echo `tput bold`"$ rvm use $DOCSRUBY"`tput sgr0` ; rvm use $DOCSRUBY
  else [[ $RVMANAGER == 'rbenv' ]]
    rbenv shell $DOCSRUBY
  fi
  echo `tput bold`'$ ruby --version'`tput sgr0` ; ruby --version
}

check_ruby() {
  if ! depcheck_ruby ; then
    error_sound ; tput setaf 3 ; read "REPLY?Required: you need Ruby $(docsrv). Would you like to install Ruby through a Ruby version manager (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      check_rbenv
    else
      echo `tput setaf 1`"Failed, you need Ruby $(docsrv)." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference.\nAborting.'
      false
    fi
  else
    echo `tput setaf 5`"Checking if your local Ruby version meets the requirement..." `tput sgr0`
    check_compare_rubies
  fi
}

check_compare_rubies() {
  RVM=ruby-$(docsrv)
  echo "GitLab Docs (remote master) version is `tput bold`ruby-$(docsrv)`tput sgr0`."
  echo "Local version is `tput bold`$(localrv)`tput sgr0`."

  if [[ $(localrv) = $RVM ]] ; then
    echo `tput setaf 2`'Rubies match.'`tput sgr0`
  else
    echo `tput setaf 1`'Failed, Rubies do not match.'`tput sgr0`
    error_sound ; tput setaf 3 ; read "REPLY?Recommended: Would you like to install Ruby version $(docsrv) (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      install_ruby
    else
      error_sound ; echo `tput setaf 1`"Failed, you need Ruby $(docsrv)." `tput sgr0` 'See https://www.ruby-lang.org/en/ for reference. \n Aborting.'
      false
    fi
  fi
}

# Install Docs Ruby with either rvm or rbenv
install_ruby() {
  if [[ $RVMANAGER == 'rvm' ]] ; then
    rvm-install-ruby
  elif [[ $RVMANAGER == 'rbenv' ]]; then
    rbenv_install_ruby
  else
    check_rbenv
  fi
}
