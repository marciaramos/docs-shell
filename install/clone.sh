# Script for ZSH

# Test file (docs test files)
test_install_clone() {
  echo 'install/clone.sh ok'
}

# ---------------------------------------------------------------- #

# Clone all repositories

# Repos associative array ('path' 'name')
# Use $k to retrieve the path and $repos_array[$k] to retrieve the name
typeset -A repos_array
repos_array=('gitlab-docs' 'GitLab Docs' 'gitlab' 'GitLab' 'gitlab-runner' 'GitLab Runner' 'omnibus-gitlab' 'Omnibus GitLab' 'charts/gitlab' 'GitLab Charts')

# Clone through SSH - GitLab team member
clone_repos_ssh() {
  tput setaf 3
  read "REPLY?All repos will be cloned into "`tput bold`"$GL"`tput sgr0 ; tput setaf 3`". This will take some time. Continue (y/n)?"`ok_sound`
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    for k in "${(@k)repos_array}" ; do
      echo `tput setaf 5`"Cloning $repos_array[$k]..."`tput sgr0`
      cd $GL
      if [[ $k == 'charts/gitlab' ]]; then
        echo `tput bold` "$ git clone git@gitlab.com:gitlab-org/$k.git charts" `tput sgr0` ; git clone git@gitlab.com:gitlab-org/$k.git charts
      else
        echo `tput bold` "$ git clone git@gitlab.com:gitlab-org/$k.git" `tput sgr0` ; git clone git@gitlab.com:gitlab-org/$k.git
      fi
      echo `tput bold` "$  git config pull.rebase false" `tput sgr0` ; git config pull.rebase false
      if (( $? )) ; then
        echo `tput setaf 1` "Failed to clone $repos_array[$k]." `tput sgr0`
      else
        tput setaf 2; echo "Cloned $repos_array[$k] into $PWD"; tput sgr0
      fi
      cd $DSHELL
    done
      # Set default paths and symlinks
      default_paths
  else
    echo "Ok. Aborting."
  fi
}

# Clone through SSH - non-GitLab team member
clone_repos_ssh_non-gl() {
  tput setaf 3
  read "REPLY?All repos will be cloned into "`tput bold`"$GL"`tput sgr0 ; tput setaf 3`". This will take some time. Continue (y/n)?"`ok_sound`
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo "Enter the namespace your projects were forked into:"`ok_sound`
    read NAMESPACE
    for k in "${(@k)repos_array}" ; do
      echo `tput setaf 5`"Cloning $repos_array[$k]..."`tput sgr0`
      cd $GL
      if [[ $k == 'charts/gitlab' ]]; then
        echo `tput bold` "$ git clone git@gitlab.com:$NAMESPACE/$k.git charts" `tput sgr0` ; git clone git@gitlab.com:$NAMESPACE/$k.git charts
      else
        echo `tput bold` "$ git clone git@gitlab.com:$NAMESPACE/$k.git" `tput sgr0` ; git clone git@gitlab.com:$NAMESPACE/$k.git
      fi
      echo `tput bold` "$  git config pull.rebase false" `tput sgr0` ; git config pull.rebase false
      if (( $? )) ; then
        echo `tput setaf 1` "Failed to clone $repos_array[$k]." `tput sgr0`
      else
        tput setaf 2; echo "Cloned $repos_array[$k] into $PWD"; tput sgr0
      fi
      cd $DSHELL
    done
      # Set default paths and symlinks
      default_paths
  else
    echo "Ok. Aborting."
  fi
}

# Clone through HTTPS
clone_repos_https() {
  tput setaf 3
  read "REPLY?All repos will be cloned into "`tput bold`"$GL"`tput sgr0 ; tput setaf 3`". This will take some time. Continue (y/n)?"`ok_sound`
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo "Enter the namespace your projects were forked into:"`ok_sound`
    read NAMESPACE
    for k in "${(@k)repos_array}" ; do
      echo `tput setaf 5`"Cloning $repos_array[$k]..."`tput sgr0`
      cd $GL
      if [[ $k == 'charts/gitlab' ]]; then
        echo `tput bold` "$ git clone https://gitlab.com/$NAMESPACE/$k.git charts" `tput sgr0` ; git clone https://gitlab.com/$NAMESPACE/$k.git charts
      else
        echo `tput bold` "$ git clone https://gitlab.com/$NAMESPACE/$k.git" `tput sgr0` ; git clone https://gitlab.com/$NAMESPACE/$k.git
      fi
      echo `tput bold` "$ git config pull.rebase false" `tput sgr0` ; git config pull.rebase false
      if (( $? )) ; then
        echo `tput setaf 1` "Failed to clone $repos_array[$k]." `tput sgr0`
      else
        tput setaf 2; echo "Cloned $repos_array[$k] into $PWD"; tput sgr0
      fi
      cd $DSHELL
    done
      # Set default paths and symlinks
      default_paths
  else
    echo "Ok. Aborting."
  fi
}

# ---------------------------------------------------------------- #

# Check SSH access
usercheck_ssh() {
  ssh -T git@gitlab.com >/dev/null
}

# Clone all repos
clone_repos() {
  if usercheck_ssh ; then # check SSH access before cloning with SSH
    tput setaf 3
    read "REPLY?Are you a GitLab team member (with at least Developer access to all projects) (y/n)?"`ok_sound`
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      clone_repos_ssh
    else
      tput setaf 3
      read "REPLY?You need all projects forked into the same namespace. Continue (y/n)?"`ok_sound`
      tput sgr0
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        clone_repos_ssh_non-gl
      else
        echo "Ok. Aborting."
      fi
    fi
  else
    tput setaf 1 ; echo "SSH connection failed." ; error_sound ; tput sgr0
    tput setaf 3
    read "REPLY?To clone the repositories via HTTPS now, you need all projects forked into the same namespace. Continue (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      clone_repos_https
    else
      echo "Ok. Aborting."
    fi
  fi
}

# `docs clone`
clone() {
  echo `tput setaf 5`"Cloning repositories..."`tput sgr0`
  clone_repos
  user_variables
  echo "PORT='3000'" >> functions/assets/user-variables.sh
  # Load new files
  source config.sh
  tput setaf 2 ; echo "User variables and paths set." ; tput sgr0
}
