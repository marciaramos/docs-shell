# Script for ZSH

# Test files (docs test files)
test_install() {
  echo 'install/install.sh ok'
}

# Test function
hello() {
  printf "Hello $USER! $WAVE \n"
}

# ---------------------------------------------------------------- #

# Install Docs Shell

# Initial command: `. ./docs.sh && access`

# Access
access(){
  tput setaf 5 ; printf "Granting shell access to Docs Shell... \n" ; tput sgr0
  echo "Granting terminal execution... \n`tput bold`$ chmod +x $CURRENT/docs.sh`tput sgr0`" ; chmod +x $CURRENT/docs.sh
  echo "Exporting path to terminal... \n`tput bold`$ export PATH=$PATH:$CURRENT`tput sgr0`" ; export PATH=$PATH:$CURRENT
  echo "Done."
  THIS=$CURRENT
  # Source temporary alias
  source functions/assets/alias.zsh
  # Add permanent alias
  PERMALIAS=$(echo 'alias docs="cd '$THIS' && . ./docs.sh && "')
  if [[ $SHELL == '/bin/zsh' ]]; then
    tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
    perm_alias
  else
    set_shell
  fi
}

# Check ZSH
set_shell(){
  if grep -q 'zsh' /etc/shells ; then
    echo "ZSH is installed but not the default shell."
    ok_sound ; read "REPLY?`tput setaf 3`(Recommended) Set ZSH as the default shell (y/n)?"`tput sgr0`
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo "$ chsh -s /bin/zsh" ; chsh -s /bin/zsh
      tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
      perm_alias
    else
      echo "Changing this shell session to ZSH..."
      echo "$ zsh" ; zsh
      echo "This might cause unforeseen errors. If you change your mind, run `tput bold`$ chsh -s /bin/zsh`tput sgr0` to make it the default shell."
      tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
      perm_alias
    fi
  else
    echo "`tput setaf 1`Houston, we have a problem! ZSH isn't installed.`tput sgr0`." ; error_sound
    read "REPLY?`tput setaf 3`(Required) Would you like install ZSH now (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      pre-install
      tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
      perm_alias
    else
      echo "ZSH is a hard requirement for Docs Shell. Using a different shell might cause unforeseen errors."
      echo "You can install ZSH later by running `tput bold`$ brew install zsh`tput sgr0`. Then restart the installation process from zero."
      tput setaf 1 ; echo "Aborting." ; tput sgr0
      return false
    fi
  fi
}

# Permanent alias
perm_alias() {
  printf "\n$PERMALIAS" >> ~/.zshrc
  source ~/.zshrc
  echo "Permanent alias added to ~/.zshrc"
}

# Compile and preview docs
first_compile(){
  go_docs
  echo `tput setaf 5`"Bundling site dependecies..." `tput sgr0`
  echo `tput bold`"$ bundle "_"$DBUN"_" install --quiet"`tput sgr0` ; bundle '_'$DBUN'_' install --quiet
  echo `tput setaf 5`"Compiling the docs site..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc compile" `tput sgr0`; bundle exec nanoc compile
  ifsuccess
  echo `tput setaf 5`"Starting live preview..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; bundle exec nanoc live -p $PORT | open_preview
}

# Update Docs Shell
shell() {
  go_shell
  if [[ $1 == '--update' ]]; then
    git_checkout_master
    echo  `tput bold` '$ git pull upstream master' `tput sgr0` ; git pull upstream master
    echo  `tput bold` '$ git push origin master' `tput sgr0` ; git push origin master
    dep
    source config.sh
  fi
}

# Install `docs install`

install() {
  printf "Hello `tput setaf 5`$USER`tput sgr0`! ${WAVE}\nBefore we begin the installation process, make sure your terminal is in the docs-shell directory:\nI'm in `tput bold`$(pwd).`tput sgr0`\n"
  echo "If it's not the correct folder, abort the process by pressing control + c on your keyboard and run `tput bold`$ docs install`tput sgr0` when you're ready."
  # Clone repos or setup current paths and create symlinks
  tput setaf 3 ; read "REPLY?Do you have `tput bold`all repos already cloned`tput sgr0``tput setaf 3` (GitLab [or GDK], Omnibus, Runner, Charts, Docs) (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      paths
    else
      clone
    fi
  # Dependencies
  echo `tput setaf 5`"Dependencies check:" `tput sgr0`
  check_dependencies
  if [[ $? == 0 ]] ; then
    tput setaf 2; printf "${UNICORN} Dependencies are ready! `tput sgr0` (${NOW}) \n"
  fi
  # Installation log file
  echo "Installed on ${NOW}" > $DSHELL/.log
  # Add upstream repo
  echo `tput setaf 5`"Adding upstream..." `tput sgr0`
  echo  `tput bold` '$ git remote add upstream https://gitlab.com/marcia/docs-shell.git' `tput sgr0` ; git remote add upstream https://gitlab.com/marcia/docs-shell.git
  # Pull all repos
  pull
  # Compile and preview
  first_compile
}
