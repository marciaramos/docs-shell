# Test files (docs test files)
test_symlinks() {
  echo 'install/symlinks.sh ok'
}

# ---------------------------------------------------------------- #

# Symlinks

symlink_gitlab() {
  rm $DOC/content/ee
  echo `tput setaf 5`"Symlinking GitLab..." `tput sgr0`
  echo `tput bold` "$ ln -s $GITLAB/doc $DOC/content/ee" `tput sgr0`; ln -s $GITLAB/doc $DOC/content/ee
}

symlink_gdk() {
  rm $DOC/content/ee
  echo `tput setaf 5`"Symlinking GitLab GDK..." `tput sgr0`
  echo `tput bold` "$ ln -s $GITLAB/gitlab/doc $DOC/content/ee" `tput sgr0`; ln -s $GITLAB/gitlab/doc $DOC/content/ee
}

symlink_omnibus() {
  rm $DOC/content/omnibus # remove old symlink
  echo `tput setaf 5`"Symlinking Omnibus..." `tput sgr0`
  echo `tput bold` "$ ln -s $OMN/doc $DOC/content/omnibus" `tput sgr0`; ln -s $OMN/doc $DOC/content/omnibus
}

symlink_runner() {
  rm $DOC/content/runner
  echo `tput setaf 5`"Symlinking Runner..." `tput sgr0`
  echo `tput bold` "$ ln -s $RUN/docs $DOC/content/runner" `tput sgr0`; ln -s $RUN/docs $DOC/content/runner
}

symlink_charts() {
  rm $DOC/content/charts
  echo `tput setaf 5`"Symlinking Charts..." `tput sgr0`
  echo `tput bold` "$ ln -s $CHAR/doc $DOC/content/charts" `tput sgr0`; ln -s $CHAR/doc $DOC/content/charts
}

symlink_check_all() {
  tput bold ; echo `tput setaf 3`"Check if all repos are listed:" `tput sgr0`
  echo `tput bold` "$ ls -la $DOC/content/" `tput sgr0`; ls -la $DOC/content/
}

symlinks() {
  symlink_omnibus
  symlink_runner
  symlink_charts
}
