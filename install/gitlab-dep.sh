# Test file (docs test files)
test_install_gl_dep() {
  echo 'install/gitlab-dep.sh ok'
}

# ---------------------------------------------------------------- #

# Compare GitLab Docs and GitLab Rubies

# Compare GitLab's and Docs' rubies
gitlab_ruby_compare() {
  GLRUBY=$(curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/master/.ruby-version > .gitlab-ruby && cat .gitlab-ruby)
  if (grep 'GLRUBY' functions/assets/variables.sh >/dev/null) ; then
    sed -i '' '/GLRUBY/d' functions/assets/variables.sh
  fi
  echo "GLRUBY='$GLRUBY'" >> functions/assets/variables.sh
  DOCSRUBY=$(docsrv)
  if [[ $GLRUBY != $DOCSRUBY ]]; then
    echo `tput setaf 1`"Houston, we have a problem.`tput sgr0` GitLab is using Ruby $GLRUBY and GitLab Docs, Ruby $DOCSRUBY. Please ask someone from the Docs Team to update."
    return
  fi
  rm .gitlab-ruby
}

# Compare GitLab Docs and GitLab Bundler

gitlab_bundler_compare() {
  curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/master/Gemfile.lock > .gitlab-lock
  GLBUN=$(awk '/BUNDLED WITH/{getline; $1=$1 ; print}' .gitlab-lock)
  if (grep 'GLBUN' functions/assets/variables.sh >/dev/null) ; then
    sed -i '' '/GLBUN/d' functions/assets/variables.sh
  fi
  echo "GLBUN='$GLBUN'" >> functions/assets/variables.sh
  DBUN=$(bundler_version)
  if [[ $GLBUN != $DBUN ]]; then
    echo `tput setaf 1`"Houston, we have a problem.`tput sgr0` GitLab is using Bundler $GLBUN and GitLab Docs, Bundler $DBUN. Please ask someone from the Docs Team to update."
  fi
  rm .gitlab-lock
}

# ---------------------------------------------------------------- #

# Generate Gemfile
gemfile() {
  if [[ -e Gemfile ]]; then
    sed -i '' "s%ruby .*%ruby \"$GLRUBY\"%g" Gemfile
  else
    printf "source \"https://rubygems.org\"\nruby \"$GLRUBY\"\n" > Gemfile
  fi
}

# ---------------------------------------------------------------- #

# haml_lint dependencies

# Curl GitLab's Gemfile to a temp file
gl-gemfile() {
  curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/master/Gemfile > .gitlab-gemfile
}

# Cat "rails" line from GL Gemfile
gitlab_rails(){
  awk 'NR==3' .gitlab-gemfile
}

# Add Rails version to Gemfile
glrails_version() {
  GLRAILS=$(gitlab_rails)
  printf $GLRAILS >> Gemfile
}

# Add gem or replace with the current gitlab's master gem version
gem_rails() {
  RAILS="gem 'rails'"
  if (grep "$RAILS" Gemfile >/dev/null) ; then
    sed -i "" "/$RAILS/d" Gemfile
    echo `tput setaf 5`"Updating Rails version..." `tput sgr0`
    glrails_version
  else
    echo `tput setaf 5`"Adding Rails to Gemfile..." `tput sgr0`
    glrails_version
  fi
}

# ---------------------------------------------------------------- #

# haml-lint - content linter for GitLab repo only (checks app/views links and anchors from the UI to /help)

# Check haml_lint from /gitlab/ master Gemfile
haml_lint_version() {
  HAMLLINT=$(curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/master/Gemfile | sed -n '/haml_lint/p')
  echo "\n$HAMLLINT" >> Gemfile
}

# Add Gem haml_lint or replace with the current gitlab's master gem version
gem_haml_lint() {
  if (grep 'haml_lint' Gemfile >/dev/null) ; then
    sed -i '' '/haml_lint/d' Gemfile
    echo `tput setaf 5`"Updating haml-lint version..." `tput sgr0`
    haml_lint_version
  else
    echo `tput setaf 5`"Adding haml-lint to Gemfile..." `tput sgr0`
    haml_lint_version
  fi
}

# ---------------------------------------------------------------- #

# Check GitLab dependencies

gl_dep() {
  # Compare GitLab and GL Docs rubies
  gitlab_ruby_compare
  # Remove current Gemfile.lock before updating
  if [[ -e Gemfile.lock ]]; then
    rm Gemfile.lock
  fi
  # Compare GitLab and GL Docs Bundler
  gitlab_bundler_compare
  # Source new variables
  source functions/assets/variables.sh
  # Generate new Gemfile
  gemfile
  echo `tput setaf 5`"Fetching gems from GitLab's Gemfile..."`tput sgr0`
  # Cat dependencies
  gl-gemfile
  gem_rails
  gem_haml_lint
  if [[ $DGDK == '0' ]]; then
    echo `tput setaf 5`"Bundling all GitLab gems. This might take a while..."`tput sgr0`
    echo `tput bold`"$ BUNDLE_GEMFILE=.gitlab-gemfile bundle install"`tput sgr0` ; BUNDLE_GEMFILE=.gitlab-gemfile bundle install
    # Remove temp file
    rm .gitlab-gemfile
  else
    echo `tput setaf 5`"Installing GitLab dependencies required for haml-lint..."`tput sgr0`
    echo `tput bold`"$ bundle "_"$GLBUN"_" install"`tput sgr0` ; bundle '_'$GLBUN'_' install
    # Remove temp file
    rm .gitlab-gemfile
  fi
}
