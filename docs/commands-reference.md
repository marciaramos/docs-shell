# Commands reference

A list of `docs [option]` commands available.

## Installation

```bash
# Test or trigger `docs`:
docs hello

# Docs Shell usage:
docs --help

# Installation:
docs install # option to clone all repos or set up custom paths
docs pre-install # option to check and install ZSH, iTerm2, oh-my-zsh
```

## Maintenance

```bash
# Change paths:
docs paths # Change paths to existing repos
docs paths --reset # reset to default (cloned) paths

# Check dependencies:
docs dep
# Update and check dependencies:
docs dep --update

# Update Docs Shell - pulls from upstream master and pushes to origin master
docs shell --update
```

## Linting

```bash
# Lint:
docs lint # lints both content and links
# - Lint options:
docs lint --content | -c # lint content
docs lint --nanoc   | -n # lint gitlab-docs

# Lint content options:
docs lint -c [repo]
# - Example:
docs lint -c gitlab
# - Lint content repo options:
docs lint -c [repo] [linter]
# - Examples:
docs lint -c gitlab markdown
docs lint -c gitlab vale
# - Lint GitLab UI app/views links
docs lint -c gitlab views

# Lint Nanoc options:
docs lint -n
# Options:
docs lint -n anchors # lint for broken anchors
docs lint -n links # lint for broken internal links
docs lint -n elinks # lint for broken external links

# gitlab-docs development linters
docs lint --dev | -d # lint dev linters
docs lint -d [linter] # lint specific linter
# Examples:
docs lint -d yaml
docs lint -d scss
```

## Compiling

```bash
# Compile:
docs compile
docs compile live # Compile and live preview
docs compile live lint # Compile, live preview, lint

# Preview GitLab Docs:
docs live

# Recompile:
docs recompile
docs recompile lint # recompile and lint content and nanoc
docs recompile lint live # recompile, lint content and nanoc, preview docs
```

## Working on content

```bash
# Pull repos
docs pull # checkout master, pull all repos (housekeeping)
docs pull [repo] [repo] ... [repo] # checkout master, pull
# - Examples:
docs pull gitlab # pull GitLab
docs pull gitlab runner # Pull GitLab and GitLab Runner
docs pull docs # Pull GitLab Docs

# Create new branch
docs branch [repo]
# - Example:
docs branch # creates a new branch where the terminal is at
# - Example:
docs branch gitlab # goes to and creates a new branch in the gitlab repo

# Commit and push
# - Example:
docs commit # commit to current branch
# - Example:
docs commit -p # commit and push to current branch
docs commit [repo] -p # commit to [repo] and push [-p] changes to current branch
# - Example:
docs commit gitlab # commit to gitlab repo
docs commit gitlab -p # commit to gitlab repo, push to the current branch

# Reset repository (checkout master and git reset --hard origin/master)
docs reset [repo]
# - Examples:
docs reset gitlab # resets `gitlab`
docs reset gitlab docs # resets `gitlab` and `docs`

# Similar to `docs pull` commands, more granular
docs update
docs update --skip # updates all repos except GitLab
docs update -h # (housekeeping)
docs update -h -s # (housekeeping + stash EE)
docs update -a # (all doc/ repos quick + gitlab-docs)
docs update -h -a #(housekeeping all)
```

## Nanoc

```bash
# Nanoc direct commands
docs nanoc compile
docs nanoc live

# Kill nanoc live
docs kill
```

## Navigation

```bash
# Navigation
docs go_gitlab
docs go_docs
docs go_runner
docs go_omnibus
docs go_charts
docs go_shell
```
