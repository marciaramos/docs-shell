# References

- Style output text: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
- Emojis: https://www.iemoji.com/ (use UTF-8 Octal Bytes code)
- Scripts:
  - https://www.taniarascia.com/how-to-create-and-use-bash-scripts/
  - https://bash.cyberciti.biz/guide/The_case_statement
- Syntax/style guide https://google.github.io/styleguide/shellguide.html
- Parallel:
  - https://www.slashroot.in/how-run-multiple-commands-parallel-linux
  - https://www.cyberciti.biz/faq/how-to-run-command-or-code-in-parallel-in-bash-shell-under-linux-or-unix/
- Flags: 
  - https://stackoverflow.com/questions/7069682/how-to-get-arguments-with-flags-in-bash
  - https://pretzelhands.com/posts/command-line-flags
  - https://jonalmeida.com/posts/2013/05/26/different-ways-to-implement-flags-in-bash/
- Case: https://bash.cyberciti.biz/guide/The_case_statement
- Check if file exists: https://linuxize.com/post/bash-check-if-file-exists/
- Add content to file: https://www.cyberciti.biz/faq/linux-append-text-to-end-of-file/
- Check RubyGems: https://serverfault.com/questions/391621/checking-if-a-ruby-gem-is-installed-from-bash-script
- Conditionals from result true/false: https://unix.stackexchange.com/questions/22726/how-to-conditionally-do-something-if-a-command-succeeded-or-failed
- Which Ruby: https://blog.arkency.com/which-ruby-version-am-i-using-how-to-check/
- Compile package with `make`: http://www.ee.surrey.ac.uk/Teaching/Unix/unix7.html
- Array: https://www.cyberciti.biz/faq/bash-for-loop-array/
- ZSH assossiative array: https://superuser.com/questions/737350/iterating-over-keys-or-k-v-pairs-in-zsh-associative-array
- Command succeded? https://www.oreilly.com/library/view/bash-cookbook/0596526784/ch04s02.html
- File permissions (`chmod` read, write, execute):
  - https://help.ubuntu.com/community/FilePermissions
  - https://blog.ssdnodes.com/blog/linux-permissions/

## Color matrix

Usage:

```bash
echo `tput setaf 3` "Hello world..." `tput sgr0`
echo `tput bold` "$ brew hello world" `tput sgr0`; brew hello world
```

```bash
# 0               black
# 1               red
# 2               green
# 3               yellow
# 4               blue
# 5               magenta
# 6               cyan
# 7               white
```

## Text style

```bash
tput bold       Bold text
tput sgr0       Normal text
tput smul       Enable underline mode
tput rmul       Disable underline mode
tput sgr0       Reset text format to the terminal\'s default
echo ""         Empty line
```

## Add alias

```bash
alias docs="cd /Users/ramosmd/GitLab/docs-shell && . ./docs.sh && "
```

## Git

- Do `git branch --set-upstream-to=origin/master` for `git pull`.
- Do `git remote set-url origin git@gitlab.com:marcia/docs-shell.git` for `git push`

## See processes

```bash
ps aux
```

## Cute little things

```bash
tput bel                     # Play a bel
UNICORN='\360\237\246\204'   # Outputs unicorn emoji UTF-8 Octal Bytes
```

## Run specific function from file

```bash
bash file-name.sh function
```

## Status

```bash
STATUS=$?
if ! $(exit $STATUS); then
    echo "command failed"
fi
```

[Ref](https://stackoverflow.com/a/43840545)

### Alternative

```bash
ifsuccess() {
  if [[ $? == 0 ]] ; then
    tput bold
    tput setaf 2; printf "Success `tput sgr0` (${NOW}) \n"
    tput sgr0
  else
    tput bold
    tput setaf 1; printf "Failed `tput sgr0` (${NOW}) \n"
    tput sgr0
  fi
}
```

## Open new iTerm window or tab

```shell
open -a iTerm .
```

## OS type

```shell
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # ...
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
elif [[ "$OSTYPE" == "win32" ]]; then
  # I'm not sure this can happen.
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  # ...
else
  # Unknown.
fi
```

[Ref](https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script)

--------------------------------------------------

_GitLab Docs Shell - Project made by [Marcia Ramos](https://gitlab.com/marcia)
with :heart: for GitLab Docs._

_Note: pls don't mind any newbieness of this project; I'm learning by doing._

